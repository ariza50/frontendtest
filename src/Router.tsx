import * as React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import history from './history';
import { ProductDashboardPage } from './modules/products/pages/ProductDashboard.page';
import { ProductDetailPage } from './modules/products/pages/ProductDetail.page';


export default (
  <Router history={history}>
    <Switch>
      <Route path="/product/:id" component={ProductDetailPage}/>
      <Route path="/products" component={ProductDashboardPage}/>
      <Route exact path="/">
        <Redirect to="/products" />
      </Route>
    </Switch>
  </Router>
);
