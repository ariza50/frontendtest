import { AxiosPromise } from 'axios';
import { Resource } from '../../../../../infrastructure/services/api-client/Resource';
import { ProductApiResponse } from './Product.apiResponse';

class ProductIndexResourceSingleton extends Resource {

  public static route = '/api/product';

  constructor() {
    super(ProductIndexResourceSingleton.route);
  }

  private static _instance: ProductIndexResourceSingleton;

  public static get instance(): ProductIndexResourceSingleton {
    return this._instance || (this._instance = new this());
  }

  public index(): AxiosPromise<Array<ProductApiResponse>> {
    return this.httpClient
      .get(this.route);
  }
}

export const ProductIndexResource = ProductIndexResourceSingleton.instance;
