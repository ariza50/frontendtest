export interface ProductApiResponse {
  id: number;
  brand: string;
  model: string;
  imgUrl: string;
  price: string;
}
