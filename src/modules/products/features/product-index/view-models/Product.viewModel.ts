import { ProductApiResponse } from '../resources/Product.apiResponse';

export class ProductViewModel {
  id: number;
  brand: string;
  model: string;
  imgUrl: string;
  price: string;

  constructor(apiResponse: ProductApiResponse) {
    this.id = apiResponse.id;
    this.brand = apiResponse.brand;
    this.model = apiResponse.model;
    this.imgUrl = apiResponse.imgUrl;
    this.price = apiResponse.price;
  }
}
