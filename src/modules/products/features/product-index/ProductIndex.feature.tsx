import autobind from 'autobind-decorator';
import React from 'react';
import { Card } from '../../../../infrastructure/components/card/Card.component';
import { SearchInput } from '../../../../infrastructure/components/inputs/search-input/SearchInput.component';
import { CardLoader } from '../../../../infrastructure/components/skeleton-loader/card-loader.component';
import { ProductIndexResource } from './resources/ProductIndex.resource';
import { ProductViewModel } from './view-models/Product.viewModel';

interface Props {
}

interface State {
  products?: ProductViewModel[];
  filteredProducts?: ProductViewModel[];
}

@autobind
export class ProductIndexFeature extends React.Component<Props, State> {

  public readonly state: State = {
    products: undefined,
  };

  public componentDidMount() {
    ProductIndexResource.index().then(response => {
      const products = response.data.map(productApiResponse => new ProductViewModel(productApiResponse));
      this.setState({ products, filteredProducts: products });
    });
  }

  public render(): React.ReactNode {

    return (
      <>
        <SearchInput
          label="Search Product"
          onChange={(value) => this.searchProduct(value)}
        />
        {
          this.state.filteredProducts === undefined || this.state.filteredProducts.length === 0 ?
            <div className="flex flex-row grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-4 md:grid-cols-2 mt-4">
              {ProductIndexFeature.getCardLoaders()}
            </div> :
          <div className="flex flex-row grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-4 md:grid-cols-2">
            {this.state.filteredProducts.map((product, colIdx) => (
              <Card
                key={colIdx}
                product={product}
              />
            ))}
          </div>
        }
      </>
    );
  }

  private static getCardLoaders(): React.ReactNode {
    let batchLoaders: React.ReactNode[] = [];
    for (let i = 0; i < 8; i++) {
      batchLoaders.push(<CardLoader key={i}/>);
    }
    return batchLoaders;
  }

  private searchProduct(value: string): void {
    const filteredProducts = this.state.products?.filter(product =>
      product.brand.toLowerCase().includes(value.toLowerCase()) ||
      product.model.toLowerCase().includes(value.toLowerCase()));
    this.setState({
      filteredProducts: filteredProducts,
    });
  }
}
