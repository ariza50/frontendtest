import { OptionApiResponse, ProductDetailApiResponse } from '../resources/ProductDetail.apiResponse';

class OptionViewModel {
  value: string;
  label: string;

  constructor(apiResponse: OptionApiResponse) {
    this.value = apiResponse.code;
    this.label = apiResponse.name;
  }
}

export class ProductDetailViewModel {
  id: number;
  brand: string;
  model: string;
  imgUrl: string;
  price: string;
  cpu: string;
  ram: string;
  os: string;
  displayResolution: string;
  battery: string;
  primaryCamera: string | string[];
  secondaryCamera: string | string[];
  dimensions: string;
  weight: string;
  availableColors: OptionViewModel[];
  availableStorages: OptionViewModel[];

  constructor(apiResponse: ProductDetailApiResponse) {
    this.id = apiResponse.id;
    this.brand = apiResponse.brand;
    this.model = apiResponse.model;
    this.imgUrl = apiResponse.imgUrl;
    this.price = apiResponse.price;
    this.cpu = apiResponse.cpu;
    this.ram = apiResponse.ram;
    this.os = apiResponse.os;
    this.displayResolution = apiResponse.displayResolution;
    this.battery = apiResponse.battery;
    this.primaryCamera = apiResponse.primaryCamera;
    this.secondaryCamera = apiResponse.secondaryCmera;
    this.dimensions = apiResponse.dimentions;
    this.weight = apiResponse.weight;
    this.availableColors = apiResponse.options.colors.map(color => new OptionViewModel(color));
    this.availableStorages = apiResponse.options.storages.map(storage => new OptionViewModel(storage));
  }

  public getPrimaryCameraDescription(camera: string | string[]): string {
    let description = '';
    if (Array.isArray(camera)) {
      description += camera.map(descriptionItem => descriptionItem);
      return description;
    }
    return camera;
  }
}
