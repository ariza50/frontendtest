import autobind from 'autobind-decorator';
import React from 'react';
import { CardDescription } from '../../../../infrastructure/components/card-description/CardDescription.component';
import { Select } from '../../../../infrastructure/components/select/Select.component';
import { ProductDetailResource } from './resources/ProductDetail.resource';
import { ProductToCartApiRequest } from './resources/ProductToCart.apiRequest';
import { ProductDetailViewModel } from './view-models/ProductDetail.viewModel';

interface Props {
  productId: string;

  onChange(value: string): void;
}

interface State {
  product?: ProductDetailViewModel;
  selectedStorageCode?: string;
  selectedColorCode?: string
  validSelectedStorage: boolean;
  validSelectedColor: boolean;
}

@autobind
export class ProductDetailFeature extends React.Component<Props, State> {

  public readonly state: State = {
    product: undefined,
    validSelectedStorage: false,
    validSelectedColor: false,
  };

  public componentDidMount() {
    ProductDetailResource.get(this.props.productId).then(response => {
      const product = new ProductDetailViewModel(response.data);
      this.setState({ product });
      if (product.availableColors.length === 1) {
        this.setState({selectedColorCode: product.availableColors[0].value})
      }
      if (product.availableStorages.length === 1) {
        this.setState({selectedStorageCode: product.availableStorages[0].value})
      }
    });
  }

  public render(): React.ReactNode {
    if (this.state.product === undefined) {
      return null;
    }
    return (
      <div className="flex flex-row items-center mt-5">
        <div className="w-1/2">
          <img
            className="w-1/2"
            style={{ display: 'unset' }}
            src={this.state.product.imgUrl}
            alt="phone"
          />
        </div>
        <div className="w-1/2">
          <div className="flex flex-col">
            <CardDescription
              productDetails={this.state.product}
            />
            {this.renderActionForm()}
          </div>
        </div>
      </div>
    );
  }

  private renderActionForm(): React.ReactNode {
    return (
      <div className="mt-8 mb-16 flex flex-col justify-center bg-white shadow overflow-hidden sm:rounded-lg">

        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <div className=" bg-white sm:rounded-lg">
            <div className="space-y-4 mt-4 mb-4">
              <div>
                <label htmlFor="email" className="block text-sm font-medium text-gray-700 text-left">
                  Storage
                </label>
                <div className="mt-1">
                  <Select
                    name="storage"
                    required={true}
                    placeholder="Select a storage"
                    selected={this.state.selectedStorageCode}
                    options={this.state.product!.availableStorages}
                    validation={(valid) => this.setState({validSelectedStorage: valid})}
                    onChange={(value) => this.setState({ selectedStorageCode: value! })} />
                </div>
              </div>

              <div>
                <label htmlFor="email" className="block text-sm font-medium text-gray-700 text-left">
                  Color
                </label>
                <div className="mt-1">
                  <Select
                    name="color"
                    placeholder="Select a color"
                    required={true}
                    validation={(valid) => this.setState({validSelectedColor: valid})}
                    options={this.state.product!.availableColors}
                    selected={this.state.selectedColorCode}
                    onChange={(value) => this.setState({ selectedColorCode: value! })} />
                </div>
              </div>

              <div>
                <button
                  onClick={() => this.addToCart()}
                  type="submit"
                  className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 disabled"
                >
                  Add to cart
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  private addToCart(): void {
    if (
      this.state.product === undefined ||
      this.state.selectedStorageCode === undefined ||
      this.state.selectedColorCode === undefined
    ) {
      alert('Please select Storage and Color to add to cart.');
      return;
    }
    const request: ProductToCartApiRequest = {
      id: this.props.productId,
      colorCode: this.state.selectedColorCode,
      storageCode: this.state.selectedStorageCode
    }
    ProductDetailResource.addToCart(request).then(response => {
      this.props.onChange(response.data.count);
    })
  }
}
