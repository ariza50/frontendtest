
export interface ProductDetailApiResponse {
  id: number;
  brand: string;
  model: string;
  imgUrl: string;
  price: string;
  cpu: string;
  ram: string;
  os: string;
  displayResolution: string;
  battery: string;
  primaryCamera: string[];
  secondaryCmera: string[];
  dimentions: string;
  weight: string;
  options: {
    colors: OptionApiResponse[];
    storages: OptionApiResponse[];
  }
}

export interface OptionApiResponse {
  code: string;
  name: string;
}
