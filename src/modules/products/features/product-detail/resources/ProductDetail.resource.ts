import { AxiosPromise } from 'axios';
import { Resource } from '../../../../../infrastructure/services/api-client/Resource';
import { ProductDetailApiResponse } from './ProductDetail.apiResponse';
import { ProductToCartApiRequest } from './ProductToCart.apiRequest';

class ProductDetailResourceSingleton extends Resource {

  public static route = '/api/product/:productId/';

  constructor() {
    super(ProductDetailResourceSingleton.route);
  }

  private static _instance: ProductDetailResourceSingleton;

  public static get instance(): ProductDetailResourceSingleton {
    return this._instance || (this._instance = new this());
  }

  public get(productId: string): AxiosPromise<ProductDetailApiResponse> {
    const {newUrl} = this.replaceRoute(this.route, {productId});
    return this.httpClient
      .get(newUrl);
  }

  public addToCart(request: ProductToCartApiRequest): AxiosPromise<{ count: string }> {
    const addToCartRoute = '/api/cart';
    return this.httpClient
      .post(addToCartRoute, request);
  }
}

export const ProductDetailResource = ProductDetailResourceSingleton.instance;
