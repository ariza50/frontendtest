export interface ProductToCartApiRequest {
  id: string;
  colorCode: string;
  storageCode: string;
}
