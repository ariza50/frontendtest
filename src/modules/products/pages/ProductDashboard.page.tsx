import autobind from 'autobind-decorator';
import React from 'react';
import { BreadCrumb } from '../../../infrastructure/components/breadCrumb/BreadCrumb.component';
import { Header } from '../../../infrastructure/components/header/Header.component';
import { ProductIndexFeature } from '../features/product-index/ProductIndex.feature';

const CART_BADGE = 'CART_BADGE';

interface State {
  badgeValue: string;
}

@autobind
export class ProductDashboardPage extends React.Component<State> {

  private uris = {
    '': '/products',
    'Products': '/products',
  };

  public readonly state: State = {
    badgeValue: '0',
  };

  public componentDidMount() {
    let badgeValue = localStorage.getItem(CART_BADGE);
    if (badgeValue === undefined || badgeValue === null) {
      badgeValue = '0';
    }
    this.setState({ badgeValue: badgeValue });
  }

  public render(): React.ReactNode {
    return (
      <div className="flex flex-col mx-20">
        <Header
          title="Products"
          badgeValue={this.state.badgeValue}
        />
        <BreadCrumb
          pages={this.uris}
        />
        <ProductIndexFeature/>
      </div>

    );
  }
}
