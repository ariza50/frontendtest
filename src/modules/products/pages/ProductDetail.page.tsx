import autobind from 'autobind-decorator';
import React from 'react';
import { match } from 'react-router';
import { BreadCrumb } from '../../../infrastructure/components/breadCrumb/BreadCrumb.component';
import { Header } from '../../../infrastructure/components/header/Header.component';
import { ProductDetailFeature } from '../features/product-detail/ProductDetail.feature';

const CART_BADGE = 'CART_BADGE';

interface Props {
  match: match<{ id: string }>;
}

interface State {
  badgeValue: string;
}

@autobind
export class ProductDetailPage extends React.Component<Props, State> {

  private uris = {
    '': '/products',
    'Products': '/products',
    'Product': '/product/' + this.props.match.params.id,
  };

  public readonly state: State = {
    badgeValue: '0',
  };

  public componentDidMount() {
    let badgeValue = localStorage.getItem(CART_BADGE);
    if (badgeValue === undefined || badgeValue === null) {
      badgeValue = '0';
    }
    this.setState({ badgeValue: badgeValue });
  }

  public render(): React.ReactNode {
    return (
      <div className="flex flex-col mx-20">
        <Header
          title="Products"
          badgeValue={this.state.badgeValue}
        />
        <BreadCrumb
          pages={this.uris}
        />
        <ProductDetailFeature
          productId={this.props.match.params.id}
          onChange={(value => this.updateBadge(value))}
        />
      </div>
    );
  }

  private updateBadge(value: string): void {
    localStorage.setItem('CART_BADGE', value);
    this.setState({ badgeValue: value });
  }
}
