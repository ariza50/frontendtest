import { AxiosInstance } from 'axios';
import HttpClient from './HttpClient';

export type Params = {
  [key: string]: string | number | undefined | boolean;
};

export type Data = {};

export class Resource {
  protected readonly route: string;
  protected readonly httpClient: AxiosInstance;

  constructor(route: string) {
    this.httpClient = HttpClient;
    this.route = route;
  }

  protected replaceRoute(url: string, params?: Params): { newUrl: string, newParams?: Params } {
    if (!params) {
      return { newUrl: url, newParams: params };
    }

    let newUrl = url + '';

    let newParams: Params = {};
    for (let param in params) {
      if (newUrl.includes(':' + param + '/')) {
        newUrl = newUrl.replace(':' + param + '/', params[param] + '/');
      } else {
        newParams[param] = params[param];
      }
    }

    return { newUrl: newUrl, newParams: newParams };
  }
}
