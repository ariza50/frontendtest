import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { Const } from '../../../config/Const';

export type Headers = { [key: string]: string };

interface ApplicationHeaders extends Headers {
  'Content-Type': string;
}

const headers: ApplicationHeaders = {
  'Content-Type': 'application/json',
};

let config: AxiosRequestConfig = {
  baseURL: Const.API_URL,
  headers: headers,
};

const HttpClient: AxiosInstance = Axios.create(config);

export default HttpClient;
