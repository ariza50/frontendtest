import { Listbox, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import autobind from 'autobind-decorator';
import * as React from 'react';
import { Fragment } from 'react';
import './Select.component.scss';

export interface Option {
  value: string | undefined;
  label: string;
}

interface Props {
  options: Array<Option>;
  placeholder?: string;
  name?: string;
  label?: string;
  selected?: string;
  disabled?: boolean;
  required: boolean;

  onChange(value: string | undefined): void;
  validation?(valid: boolean): void;
}

interface State {
  canValidate: boolean;
}

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ');
}

@autobind
export class Select extends React.Component<Props, State> {

  public state: State = {
    canValidate: this.props.required,
  }

  public render(): React.ReactNode {
    const selected = this.props.options.find(op => op.value === this.props.selected);
    const selectedLabel = selected ? selected.label : this.props.placeholder;
    return (
      <span className="select-component">
        <Listbox
          disabled={this.props.disabled}
          value={this.props.selected}
          onChange={(value) => {
            this.props.onChange(value);
            this.setState({canValidate: true});
            this.emitValidation();
          }}>
          {({ open }) => (
            <>
              {this.renderLabel()}
              <div className="mt-1 relative">
                {this.renderButton(selectedLabel)}
                {this.renderOptions(open)}
              </div>
            </>
          )}
        </Listbox>
      </span>
    );
  }

  public componentDidMount(): void {
    this.emitValidation();
  }

  private emitValidation(): void {
    if (!!this.props.validation) {
      this.props.validation(this.isValid());
    }
  }

  private isValid(): boolean {
    if (!this.state.canValidate) {
      return true;
    }
    if (this.props.selected === undefined) {
      return false;
    }
    
    return true;
  }

  private renderOptions(open: boolean): React.ReactNode {
    return (
      <Transition
        show={open}
        as={Fragment}
        leave="transition ease-in duration-100"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <Listbox.Options
          static
          className={`list-box-options ${this.props.disabled ? 'disabled' : ''}`}
        >
          {this.props.options.map((option, key) => this.renderOption(key, option))}
        </Listbox.Options>
      </Transition>);
  }

  private renderButton(selectedLabel: string | undefined): React.ReactNode {
    return (
      <Listbox.Button className={`list-box-button ${this.props.disabled ? 'disabled' : ''} ${!this.isValid() ? 'error' : ''}`}>
        <span className="block truncate">{selectedLabel}</span>
        <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
          <ChevronDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
        </span>
      </Listbox.Button>
    );
  }

  private renderOption(key: number, option: Option): React.ReactNode {
    return (
      <Listbox.Option
        key={key}
        className={({ active }) =>
          classNames(
            active ? 'text-gray-900 bg-gray-100' : 'text-gray-900',
            'cursor-default select-none relative py-2 pl-3 pr-9',
          )
        }
        value={option.value}
      >
        {({ selected, active }) => (
          <>
          <span className={classNames(selected ? 'font-semibold' : 'font-normal', 'block truncate')}>
            {option.label}
          </span>
            {this.renderSelected(selected, active)}
          </>
        )}
      </Listbox.Option>
    );
  }

  private renderSelected(selected: boolean, active: boolean): React.ReactNode {
    if (!selected) {
      return null;
    }
    return (
      <span
        className={classNames(
          active ? 'text-gray-900' : 'text-indigo-600',
          'absolute inset-y-0 right-0 flex items-center pr-4',
        )}
      >
      </span>
    );
  }

  private renderLabel(): React.ReactNode {
    if (!this.props.label) {
      return null;
    }
    return (
      <Listbox.Label
        className="block text-sm font-medium text-gray-700">
        {this.props.label}
      </Listbox.Label>);
  }
}
