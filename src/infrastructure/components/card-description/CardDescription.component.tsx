import autobind from 'autobind-decorator';
import React from 'react';
import { ProductDetailViewModel } from '../../../modules/products/features/product-detail/view-models/ProductDetail.viewModel';

interface Props {
  productDetails: ProductDetailViewModel;
}

@autobind
export class CardDescription extends React.Component<Props> {

  public render(): React.ReactNode {
    return (
      <div className="bg-white shadow overflow-hidden sm:rounded-lg">
        <div className="px-4 py-2 sm:px-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900">{this.props.productDetails.model}</h3>
          <p className="mt-1 max-w-2xl text-sm text-gray-500">{this.props.productDetails.brand}</p>
        </div>
        <div className="border-t border-gray-200">
          <dl>
            <div className="bg-gray-50 px-4 py-1 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Price</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.price}</dd>
            </div>
            <div className="bg-white px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">CPU</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.cpu}</dd>
            </div>
            <div className="bg-gray-50 px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">RAM</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.ram}</dd>
            </div>
            <div className="bg-white px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">OS</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.os}</dd>
            </div>
            <div className="bg-gray-50 px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Display Resolution</dt>
              <dd
                className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.displayResolution}</dd>
            </div>
            <div className="bg-gray-50 px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Battery</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.battery}</dd>
            </div>
            <div className="bg-gray-50 px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Primary Camera</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {this.props.productDetails.getPrimaryCameraDescription(this.props.productDetails.primaryCamera)}
              </dd>
            </div>
            <div className="bg-gray-50 px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Secondary Camera</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {this.props.productDetails.getPrimaryCameraDescription(this.props.productDetails.secondaryCamera)}
              </dd>
            </div>
            <div className="bg-gray-50 px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Dimensions</dt>
              <dd
                className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.dimensions}</dd>
            </div>
            <div className="bg-gray-50 px-4 py-1.5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Weight</dt>
              <dd
                className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{this.props.productDetails.weight}</dd>
            </div>
          </dl>
        </div>
      </div>
    );
  }
}
