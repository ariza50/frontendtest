import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid';
import autobind from 'autobind-decorator';
import React from 'react';

interface Pages {
  [key: string]: string;
}

interface Props {
  pages: Pages;
}

@autobind
export class BreadCrumb extends React.Component<Props> {

  public render(): React.ReactNode {
    return (
      <div className="mt-2">
        <nav className="sm:hidden" aria-label="Back">
          <a href="http://localhost:3000/home#" className="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700">
            <ChevronLeftIcon className="flex-shrink-0 -ml-1 mr-1 h-5 w-5 text-gray-400" aria-hidden="true" />
            Back
          </a>
        </nav>
        <nav className="hidden sm:flex" aria-label="Breadcrumb">
          <ol className="flex items-center space-x-4">
            {this.props.pages && Object.keys(this.props.pages).map((key, i) =>
              <li key={i}>
                <div className={i !== 0 ? 'flex items-center': ''}>
                  {
                    i !== 0 ?
                      <ChevronRightIcon className="flex-shrink-0 h-5 w-5 text-gray-400" aria-hidden="true" /> :
                      null
                  }

                  <a href={this.props.pages[key]} className="text-sm font-medium text-gray-500 hover:text-gray-700">
                    {key}
                  </a>
                </div>
              </li>,
            )}
          </ol>
        </nav>
      </div>
    );
  }
}
