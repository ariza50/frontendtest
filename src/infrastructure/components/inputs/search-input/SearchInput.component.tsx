import autobind from 'autobind-decorator';
import React from 'react';

interface Props {
  label: string;
  placeholder?: string;
  onChange(value: string): void;
}

@autobind
export class SearchInput extends React.Component<Props> {

  public render(): React.ReactNode {
    return (
      <div className="max-w-xs mt-2">
        <label htmlFor="email" className="block text-left text-sm font-medium text-gray-700">
          {this.props.label}
        </label>
        <div className="mt-1 relative rounded-md shadow-sm">
          <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
              <path fillRule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clipRule="evenodd" />
            </svg>
          </div>
          <input
            type="text"
            name="email"
            id="email"
            className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm border-gray-300 rounded-md h-8"
            placeholder={!!this.props.placeholder ? this.props.placeholder: 'Type something to search'}
            onChange={(e) => this.props.onChange(e.target.value)}
          />
        </div>
      </div>
    );
  }
}
