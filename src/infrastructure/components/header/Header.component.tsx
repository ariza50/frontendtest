import * as React from 'react';
import autobind from 'autobind-decorator';
import history from '../../../history';

interface Props {
  title: string;
  badgeValue: string
}

@autobind
export class Header extends React.Component<Props> {

  public render(): React.ReactNode {
    return (
      <div className="mt-2 md:flex md:items-center md:justify-start border-2 bg-white shadow rounded-lg h-16">
        <div
          className="flex-1 min-w-0 cursor-pointer"
          onClick={Header.goHome}
        >
          <h2 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">{this.props.title}</h2>
        </div>

        <div className="mt-4 mr-1 flex-shrink-0 flex md:mt-0 md:ml-4">
          <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" viewBox="0 0 20 20" fill="currentColor">
            <path d="M3 1a1 1 0 000 2h1.22l.305 1.222a.997.997 0 00.01.042l1.358 5.43-.893.892C3.74 11.846 4.632 14 6.414 14H15a1 1 0 000-2H6.414l1-1H14a1 1 0 00.894-.553l3-6A1 1 0 0017 3H6.28l-.31-1.243A1 1 0 005 1H3zM16 16.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM6.5 18a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" />
          </svg>
        </div>
        <div className="mt-4 mr-4 flex-shrink-0 flex md:mt-0 md:ml-1">
          <span className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-blue-500 text-white">
            {this.props.badgeValue}
          </span>
        </div>
      </div>
    );
  }

  private static goHome(): void {
    history.push('/products');
  }
}
