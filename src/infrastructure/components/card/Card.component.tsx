import autobind from 'autobind-decorator';
import React from 'react';
import history from '../../../history';
import { ProductViewModel } from '../../../modules/products/features/product-index/view-models/Product.viewModel';

interface Props {
  product: ProductViewModel
}

@autobind
export class Card extends React.Component<Props> {

  public render(): React.ReactNode {
    return (
      <div
        className="cursor-pointer"
        onClick={() => this.productDetails()}
      >
        <dl className="mt-5">
            <div
              className="relative bg-white pt-5 px-4 sm:pt-6 sm:px-6 shadow rounded-lg overflow-hidden"
            >
              <dt>

                <div className="flex-shrink-0 content-center">
                  <img
                    className=""
                    src={this.props.product.imgUrl}
                    alt=""
                  />
                </div>
              </dt>
              <dd className="ml-1 mt-2 pb-6 flex flex-col text-left items-baseline sm:pb-7">
                <div className="py-1">
                <p className="text-xl font-semibold text-gray-900">Brand: {this.props.product.brand}</p>
                </div>
                <div className="py-1">
                  <p className="text-xl font-semibold text-gray-900">Model: {this.props.product.model}</p>
                </div>
                <div className="py-1">
                  <p className="text-xl font-semibold text-gray-900">Price: {this.props.product.price}</p>
                </div>
              </dd>
            </div>
        </dl>
      </div>
    );
  }

  private productDetails(): void {
    history.push('/product/' + this.props.product.id);
  }
}
