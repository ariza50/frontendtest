import React from 'react';
import './App.css';

class App extends React.Component {
  public render(): React.ReactNode {
    return (
      <div className="App">
        {this.props.children}
      </div>
    );
  }
}

export default App;
